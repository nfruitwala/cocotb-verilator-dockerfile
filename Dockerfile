# syntax=docker/dockerfile:1
FROM ubuntu:20.04

#configure verilator
RUN apt-get update --fix-missing && apt-get install -y software-properties-common
RUN apt-get update --fix-missing && add-apt-repository ppa:deadsnakes/ppa -y
RUN apt-get update --fix-missing && apt-get install -y git perl python3.10 make autoconf g++ flex bison ccache
RUN apt-get update --fix-missing && apt-get install -y libgoogle-perftools-dev numactl perl-doc
RUN apt-get update --fix-missing && apt-get install -y libfl2  # Ubuntu only (ignore if gives error)
RUN apt-get update --fix-missing && apt-get install -y libfl-dev  # Ubuntu only (ignore if gives error)
RUN apt-get update --fix-missing && apt-get install -y zlibc zlib1g zlib1g-dev  # Ubuntu only (ignore if gives error)
RUN apt-get update --fix-missing && apt-get install -y curl
RUN git clone https://github.com/verilator/verilator --branch v4.106 --depth 1
WORKDIR ./verilator
RUN autoconf
RUN ./configure
RUN make
RUN make install
RUN ln -s /usr/bin/python3.10 /usr/bin/python
WORKDIR /

#configure python+cocotb
RUN curl -sS https://bootstrap.pypa.io/get-pip.py | python3.10
RUN python3.10 -m pip install cocotb==1.7 numpy matplotlib ipdb pytest
